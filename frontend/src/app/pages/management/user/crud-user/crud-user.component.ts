import { Component, OnInit } from '@angular/core';
import { Logger } from '../../../../services/Logger';
import { DbService } from '../../../../services/db.service';
import { User } from '../../../../models/User.model';
import { Router, ActivatedRoute } from '@angular/router';
import { BlockChainService } from '../../../../services/block-chain.service';

@Component({
  selector: 'ngx-crud-user',
  templateUrl: './crud-user.component.html',
  styleUrls: ['./crud-user.component.scss']
})
export class CrudUserComponent implements OnInit {
  
  user: User;
  currentCrudType = "add";
  submitBtnText = "Submit";
  contactsArr = [];
  processOwner = "";
  defaultSpace = " ";
  
  constructor(private _db: DbService,private router:Router, private _blockchainService: BlockChainService, private _activatedRoute: ActivatedRoute) { }
  

  ngOnInit() {
    Logger.logMsg("Enter: CRUD User Component");
    this.user = new User();
    this.fillFormFields();
    this._activatedRoute.params.subscribe(
      params => {
        Logger.logMsgObj("Params:: ", params);
        //check if edit
        if (params["crudType"] == "edit") {
          this.currentCrudType = "edit";
          this.submitBtnText = "Update";
          let userId = params["userId"];
          this._db.getSingleUser(userId).subscribe(
            data => {
              Logger.logMsgObj("Got org: ", data["data"]);
              this.user = data["data"];
              this.processOwner = this.user.contact_id;
            },
            error => {
              Logger.logMsgObj("Error:", error);
            }

          )
        }
      }
    );
  }

  
  fillFormFields() {
    this._db.getAllContacts().subscribe(
      data => {
        this.contactsArr = data["data"];
        Logger.logMsgObj("owner:: ", this.contactsArr);
      },
      error => {
        Logger.logMsg("Error: " + error);
      }
    );

  }
  
  onSubmitClick(){
    Logger.logMsgObj("user tobe saved: ", this.user);
    this.user.status = "Active";//default
    this.user.contact_id = this.processOwner;
    if (this.currentCrudType == "add") {
      // this.user._id = null;      
      this._db.saveNewUser(this.user).subscribe(
        data => {
          Logger.logMsgObj("user from db:" , data);

          let userId = data["data"]._id;
          //save to blockchain
          Logger.logMsg("Calling blockchain for userId: " + userId);
          this._blockchainService.addNewUser(userId).subscribe(
            data2 => {
              Logger.logMsgObj("reply from blockchain", data2);
            },
            err2 => {
              Logger.logMsgObj("error", err2);
            }
          );


          this.router.navigate(['/pages/management/user']);
        },
        err => {
          Logger.logMsg(err);
        }
      );
    } else {
      this._db.updateUser(this.user).subscribe(
        data => {
          Logger.logMsg(data);
          this.router.navigate(['/pages/management/user']);
        },
        err => {
          Logger.logMsg(err);
        }
      );
    }
  }
  onCancelClick(){
    this.router.navigate(['/pages/management/user']);    
  }
}
