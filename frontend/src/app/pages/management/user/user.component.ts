import { Component, OnInit } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';

import { SmartTableService } from '../../../@core/data/smart-table.service';
import { Router, ActivatedRoute } from '@angular/router';
import { DbService } from '../../../services/db.service';
import { Logger } from '../../../services/Logger';


@Component({
  selector: 'ngx-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {


  ngOnInit() {
  }

  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    hideSubHeader: true,
    noDataMessage: "No Users available",
    actions: {
      add: false,
      edit: false,
      delete: false
    },
    columns: {
      status: {
        title: 'Status',
        type: 'string',
      },
      contact_id: {
        title: 'Contact',
        type: 'string',
      },
      login_email: {
        title: 'Email',
        type: 'string',
      },
      permissions: {
        title: 'Permissions',
        type: 'string',
      }
    },
  };

  source: LocalDataSource = new LocalDataSource();

  constructor(
    private router: Router,
    private service: SmartTableService,
    private _db: DbService,
    private activatedRoute: ActivatedRoute
  ) {

    this.getAllUsers();
    // const data = this.getDataFromDb();
    // this.source.load(data);
  }


  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }

  onSearch(query: string = '') {
    this.source.setFilter([
      // fields we want to inclue in the search
      {
        field: 'name',
        search: query,
      },
      {
        field: 'jobTitle',
        search: query,
      },
      {
        field: 'organization',
        search: query,
      },
    ], false);
  }


  getAllUsers() {
    // this._db.getT();
    this._db.getAllUsers().subscribe(
      data => {
        Logger.logMsgObj("Got userss from db: ", data);
        this.source.load(data["data"]);
      },
      error => {
        Logger.logMsgObj("error: ", error);
      }
    );
  }

  onAddUserClick() {
    console.log("Add User will be called.");
    this.router.navigate(['/pages/management/user/crud-user']);
  }

  onUserRowSelect(event){
    Logger.logMsg("Inside onUserRowSelect function");
    Logger.logObj(event);
    this.router.navigate(['/pages/management/user/crud-user/modify', "edit", event.data._id]);
  }
}
