
import { Component, OnInit } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';

import { SmartTableService } from '../../../@core/data/smart-table.service';
import { DbService } from '../../../services/db.service';
import { Logger } from '../../../services/Logger';
import { Router } from '@angular/router';
import { Activity } from '../../../models/Activity.model';

@Component({
  selector: 'ngx-ticket',
  templateUrl: './ticket.component.html',
  styleUrls: ['./ticket.component.scss']
})
export class TicketComponent implements OnInit {

  activity: Activity;

  constructor(private service: SmartTableService, private _db: DbService,
    private router: Router) {
    this.getAllActivities();
  }


  ngOnInit() {
    Logger.logMsg("Enter: Ticket Component");
    this.activity = new Activity();
  }

  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    hideSubHeader: true,
    noDataMessage: "No Progressing Activities found",
    actions: {
      add: false,
      edit: false,
      delete: false
    },
    columns: {

      label: {
        title: 'Label',
        type: 'string',
      },
      process_owner: {
        title: 'Assigned Operator',
        type: 'string',
      },
      role: {
        title: 'Status',
        type: 'string',
      },
      department: {
        title: 'Importance',
        type: 'string',
      },
      created_date: {
        title: 'Due Date',
        type: 'string',
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();

  getAllActivities() {
    this._db.getAllActivities().subscribe(
      data => {
        Logger.logMsgObj("Got reply from db: ", data);
        this.source.load(data["data"]);
      },
      error => {
        Logger.logMsgObj("error: ", error);
      }
    );
  }

  getDataFromDb() {
    let data = [{
      name: 'Organization Name',
      phone: '123456',
      country: 'USA',
      web: '123@gmail.com'
    },
    {
      name: 'Organization Name 2',
      phone: '782425',
      country: 'UK',
      web: 'abc@gmail.com'
    },
    ];
    return data;
  }

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }

  onAddActivityClick() {
    Logger.logMsg("Add Activity will be called.");
    this.router.navigate(['/pages/management/ticket/crud-ticket']);
  }

  onUserRowSelect(event) {
    Logger.logMsg("Inside onUserRowSelect function");
    Logger.logObj(event);
    this.router.navigate(['/pages/management/ticket/crud-ticket/modify', "edit", event.data._id]);
  }
}

