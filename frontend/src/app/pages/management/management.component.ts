import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-components',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class ManagementComponent implements OnInit {
  ngOnInit(){
    console.log("Enter: MANAGEMENT Component");
  }
}
