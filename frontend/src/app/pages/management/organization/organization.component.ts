import { Component, OnInit } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';

import { SmartTableService } from '../../../@core/data/smart-table.service';
import { DbService } from '../../../services/db.service';
import { Logger } from '../../../services/Logger';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-organization',
  templateUrl: './organization.component.html',
  styles: [`
    nb-card {
      transform: translate3d(0, 0, 0);
    }
  `]
})
export class OrganizationComponent implements OnInit {

  constructor(private service: SmartTableService, private _db: DbService,
    private router: Router) {
    // const data = this.service.getData();
    this.getAllOrganizations();
    // const data = this.getDataFromDb();
    // this.source.load(data);
  }
  ngOnInit() {
  }

  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    hideSubHeader: true,
    noDataMessage: "No Organizations available",
    actions: {
      add: false,
      edit: false,
      delete: false
    },
    columns: {
      name: {
        title: 'Name',
        type: 'string',
      },
      phone: {
        title: 'Phone',
        type: 'string',
      },
      country: {
        title: 'Country',
        type: 'string',
      },
      website: {
        title: 'Web',
        type: 'string',
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();



  getAllOrganizations() {
    // this._db.getT();
    this._db.getAllOrganizations().subscribe(
      data => {
        Logger.logMsgObj("Got reply from db: ", data);
        this.source.load(data["data"]);
        // this.templatesArr = data["data"];
      },
      error => {
        Logger.logMsgObj("error: ", error);
      }
    );
  }

  getDataFromDb() {
    let data = [{
      name: 'Organization Name',
      phone: '123456',
      country: 'USA',
      web: '123@gmail.com'
    },
    {
      name: 'Organization Name 2',
      phone: '782425',
      country: 'UK',
      web: 'abc@gmail.com'
    },
    ];
    return data;
  }

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }

  onAddOrganizationClick() {
    Logger.logMsg("Add Organization will be called.");
    this.router.navigate(['/pages/management/organization/crud-organization']);
  }

  onUserRowSelect(event){
    Logger.logMsg("Inside onUserRowSelect function");
    Logger.logObj(event);
    // this.router.navigate(['/pages/management/organization/crud-organization']);
    this.router.navigate(['/pages/management/organization/crud-organization/modify', "edit", event.data._id]);
    // this.router.navigate(['/pages/management/organization/crud-organization/modify']);
  }
}
