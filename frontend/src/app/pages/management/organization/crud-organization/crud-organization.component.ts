import { Component, OnInit } from '@angular/core';
import { Contact } from '../../../../models/Contact.model';
import { Logger } from '../../../../services/Logger';
import { Organization } from '../../../../models/Organization.model';
import { DbService } from '../../../../services/db.service';
import { ActivatedRoute, Router } from '@angular/router';
import { BlockChainService } from '../../../../services/block-chain.service';

@Component({
  selector: 'ngx-crud-organization',
  templateUrl: './crud-organization.component.html',
  styleUrls: ['./crud-organization.component.scss']
})
export class CrudOrganizationComponent implements OnInit {

  organization: Organization;
  templatesArr = [];
  // templatesMap = new Map<string, Organization>();
  selectedTemplate: any;
  currentCrudType = "add";
  submitBtnText = "Submit";

  constructor(private _db: DbService, private _blockchainService:BlockChainService, private router:Router, private _activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    Logger.logMsg("Enter: CRUD Organization Component");
    this.organization = new Organization();
    this.fillOrganizationTemplates();

    this._activatedRoute.params.subscribe(
      params => {
        Logger.logMsgObj("Params:: ", params);
        //check if edit
        if (params["crudType"] == "edit") {
          this.currentCrudType = "edit";
          this.submitBtnText = "Update";
          let organizationId = params["organizationId"];
          this._db.getSingleOrganization(organizationId).subscribe(
            data => {
              Logger.logMsgObj("Got org: ", data["data"]);
              this.organization = data["data"];
            },
            error => {
              Logger.logMsgObj("Error:", error);
            }

          )
        }
      }
    );
  }

  onSubmitClick() {
    Logger.logMsgObj("organization to be saved: ", this.organization);
    if (this.currentCrudType == "add") {
      // this.organization._id = null;
      this.organization.is_template = false;

      this._db.saveNewOrganization(this.organization).subscribe(
        data => {
          Logger.logMsg(data);

          let organizationId = data["data"]._id;
          //save to blockchain
          Logger.logMsg("Calling blockchain for organizationId: " + organizationId);
          this._blockchainService.addNewOrganization(organizationId).subscribe(
            data2 => {
              Logger.logMsgObj("reply from blockchain", data2);
            },
            err2 => {
              Logger.logMsgObj("error", err2);
            }
          );

          this.router.navigate(['/pages/management/organization']);
        },
        err => {
          Logger.logMsg(err);
        }
      );
    } else {
      this._db.updateOrganization(this.organization).subscribe(
        data => {
          Logger.logMsg(data);
          this.router.navigate(['/pages/management/organization']);
        },
        err => {
          Logger.logMsg(err);
        }
      );
    }
    
  }

  fillOrganizationTemplates() {
    this._db.getAllOrganizationTemplates().subscribe(
      data => {
        Logger.logMsgObj("Got reply from db: ", data);
        this.templatesArr = data["data"];
      },
      error => {
        Logger.logMsgObj("error: ", error);
      }
    );

    // let org = new Organization(true);
    // org._id = "a11111111";
    // org.name = "template 1";
    // org.street = "284 - Wheeler Street";
    // org.city = "Akron";
    // org.country = "USA";
    // this.templatesArr.push(org);
    // this.templatesMap.set(org._id, org);
    // org = new Organization(true);
    // org._id = "b2222222222";
    // org.name = "template 2";
    // org.street = "789 Maryland Street";
    // org.city = "London";
    // org.country = "UK";
    // this.templatesArr.push(org);
    // this.templatesMap.set(org._id, org);
  }

  onSelectTemplate(event) {
    Logger.logMsg("selected template: ");
    Logger.logObj(this.selectedTemplate);

    delete this.selectedTemplate._id;
    this.organization = this.selectedTemplate;

  }

  onResetClick() {
    this.organization = new Organization(false);
  }

  onCancelClick(){
    this.router.navigate(['/pages/management/organization']);    
  }
}
