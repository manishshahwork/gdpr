import { NgModule } from '@angular/core';

import { ToasterModule } from 'angular2-toaster';
import { ThemeModule } from '../../@theme/theme.module';
import { ContactComponent } from './contact/contact.component';
import { OrganizationComponent } from './organization/organization.component';
import { UserComponent } from './user/user.component';
import { ManagementRoutingModule } from './management-routing.module';
import { ManagementComponent } from './management.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { CrudContactComponent } from './contact/crud-contact/crud-contact.component';
import { CrudOrganizationComponent } from './organization/crud-organization/crud-organization.component';
import { CrudUserComponent } from './user/crud-user/crud-user.component';
import { CrudDocumentComponent } from './document/crud-document/crud-document.component';
import { DocumentComponent } from './document/document.component';
import { TicketComponent } from './ticket/ticket.component';
import { CrudTicketComponent } from './ticket/crud-ticket/crud-ticket.component';


const COMPONENTS = [
    ManagementComponent,
    ContactComponent,
    OrganizationComponent,
    UserComponent,
    CrudContactComponent,
    CrudOrganizationComponent,
    CrudUserComponent,
    DocumentComponent,
    CrudDocumentComponent,
    TicketComponent,
    CrudTicketComponent
];

const SERVICES = [
  
];

const MODULES = [
  ThemeModule,
  ManagementRoutingModule,
  ToasterModule.forRoot(),
  Ng2SmartTableModule
];

@NgModule({
  imports: [
    ...MODULES,
  ],
  declarations: [
    ...COMPONENTS,
  ],
  providers: [
    ...SERVICES,
  ],
})
export class ManagementModule { }
