import { Component, OnInit } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';

import { SmartTableService } from '../../../@core/data/smart-table.service';
import { Router, ActivatedRoute } from '@angular/router';
import { DbService } from '../../../services/db.service';
import { Logger } from '../../../services/Logger';


@Component({
  selector: 'ngx-contact',
  templateUrl: './contact.component.html',
  styles: [`
    nb-card {
      transform: translate3d(0, 0, 0);
    }
  `]
})
export class ContactComponent implements OnInit {


  ngOnInit() {
  }

  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    hideSubHeader: true,
    noDataMessage : "No Contacts available",
    actions: {
      add:false,
      edit:false,
      delete:false
    },
    columns: {
      first_name: {
        title: 'First Name',
        type: 'string',
      },
      last_name: { 
        title: 'Last Name',
        type: 'string',
      },
      role: {
        title: 'Job Title',
        type: 'string',
      },
      // organization: {
      //   title: 'Organization',
      //   type: 'string',
      // },
      phone: {
        title: 'Phone',
        type: 'string',
      },
      email: {
        title: 'Email',
        type: 'string',
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();

  constructor(
    private router: Router,
    private service: SmartTableService,
    private _db:DbService,
    private activatedRoute: ActivatedRoute
  ) {

    this.getAllContacts();
    // const data = this.getDataFromDb();
    // this.source.load(data);
  }

  getDataFromDb() {
    let data = [{
      name: 'Name',
      jobTitle: 'Software Developer',
      organization: 'Org. 1',
      phone: '1213456',
      email: '123@gmail.com'
    },
    {
      name: 'Name2',
      jobTitle: 'Construction Developer',
      organization: 'Org. 2',
      phone: '8939210',
      email: '4anasfd@gmail.com'
    },

    ];
    return data;
  }

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }

  onSearch(query: string = '') {
    this.source.setFilter([
      // fields we want to inclue in the search
      {
        field: 'name',
        search: query,
      },
      {
        field: 'jobTitle',
        search: query,
      },
      {
        field: 'organization',
        search: query,
      },
    ], false);
    // second parameter specifying whether to perform 'AND' or 'OR' search
    // (meaning all columns should contain search query or at least one)
    // 'AND' by default, so changing to 'OR' by setting false here
  }


  getAllContacts(){
    // this._db.getT();
    this._db.getAllContacts().subscribe(
      data=>{
        Logger.logMsgObj("Got contacts from db: ", data);
        this.source.load(data["data"]);
        // this.templatesArr = data["data"];
      }, 
      error => {
        Logger.logMsgObj("error: " , error);
      }
    );
  }

  onAddContactClick(){
    console.log("Add Contact will be called.");
    Logger.logMsgObj("activated:::" , this.activatedRoute.snapshot);
    this.router.navigate(['/pages/management/contact/crud-contact']);
  }

  onContactRowSelect(event){
    Logger.logMsg("Inside onContactRowSelect function");
    Logger.logObj(event);
    this.router.navigate(['/pages/management/contact/crud-contact/modify', "edit", event.data._id]);
  }
}
