import { Component, OnInit } from '@angular/core';
import { Contact } from '../../../../models/Contact.model';
import { Logger } from '../../../../services/Logger';
import { DbService } from '../../../../services/db.service';
import { Router, ActivatedRoute } from '@angular/router';
import { BlockChainService } from '../../../../services/block-chain.service';

@Component({
  selector: 'ngx-crud-contact',
  templateUrl: './crud-contact.component.html',
  styleUrls: ['./crud-contact.component.scss']
})
export class CrudContactComponent implements OnInit {
  
  contact: Contact;
  currentCrudType = "add";
  submitBtnText = "Submit";
  
  constructor(private _db: DbService,
    private router:Router, 
    private _activatedRoute: ActivatedRoute,
    private _blockchainService: BlockChainService
    ) { }
  

  ngOnInit() {
    Logger.logMsg("Enter: CRUD Contact Component");
    this.contact = new Contact();
    
    this._activatedRoute.params.subscribe(
      params => {
        Logger.logMsgObj("Params:: ", params);
        //check if edit
        if (params["crudType"] == "edit") {
          this.currentCrudType = "edit";
          this.submitBtnText = "Update";
          let contactId = params["contactId"];
          this._db.getSingleContact(contactId).subscribe(
            data => {
              Logger.logMsgObj("Got org: ", data["data"]);
              this.contact = data["data"];
            },
            error => {
              Logger.logMsgObj("Error:", error);
            }

          )
        }
      }
    );

  }

  onSubmitClick(){
    Logger.logMsgObj("Contact tobe saved: ", this.contact);
    if (this.currentCrudType == "add") {
      // this.contact._id = null;      
      this._db.saveNewContact(this.contact).subscribe(
        data => {
          Logger.logMsg(data);

          let contactId = data["data"]._id;
          //save to blockchain
          Logger.logMsg("Calling blockchaing for contactid: " + contactId);
          this._blockchainService.addNewContact(contactId).subscribe(
            data2 => {
              Logger.logMsgObj("reply from blockchain", data2);
            },
            err2 => {
              Logger.logMsgObj("error", err2);
            }
          );
          
          this.router.navigate(['/pages/management/contact']);
        },
        err => {
          Logger.logMsg(err);
        }
      );
    } else {
      this._db.updateContact(this.contact).subscribe(
        data => {
          Logger.logMsg(data);
          this.router.navigate(['/pages/management/contact']);
        },
        err => {
          Logger.logMsg(err);
        }
      );
    }
  }

  onCancelClick(){
    this.router.navigate(['/pages/management/contact']);    
  }
}
