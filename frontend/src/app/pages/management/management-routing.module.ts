import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ManagementComponent } from './management.component';
import { ContactComponent } from './contact/contact.component';
import { UserComponent } from './user/user.component';
import { OrganizationComponent } from './organization/organization.component';
import { CrudContactComponent } from './contact/crud-contact/crud-contact.component';
import { CrudOrganizationComponent } from './organization/crud-organization/crud-organization.component';
import { CrudUserComponent } from './user/crud-user/crud-user.component';
import { DocumentComponent } from './document/document.component';
import { CrudDocumentComponent } from './document/crud-document/crud-document.component';
import { TicketComponent } from './ticket/ticket.component';
import { CrudTicketComponent } from './ticket/crud-ticket/crud-ticket.component';


const routes: Routes = [{
  path: '',
  component: ManagementComponent,

  children: [
    {
      path: 'contact/crud-contact',
      component: CrudContactComponent,
    },
    {
      path: 'contact',
      component: ContactComponent,
    },
    {
      path: 'user',
      component: UserComponent,
    },
    {
      path: 'user/crud-user',
      component: CrudUserComponent,
    },
    {
      path: 'organization',
      component: OrganizationComponent,
    },
    {
      path: 'organization/crud-organization',
      component: CrudOrganizationComponent,
    },
    { 
      path: 'organization/crud-organization/modify/:crudType/:organizationId', 
      component: CrudOrganizationComponent 
    },
    { 
      path: 'contact/crud-contact/modify/:crudType/:contactId', 
      component: CrudContactComponent 
    },
    { 
      path: 'user/crud-user/modify/:crudType/:userId', 
      component: CrudUserComponent 
    },
    {
      path: 'document',
      component: DocumentComponent,
    },
    {
      path: 'document/crud-document',
      component: CrudDocumentComponent,
    },
    { 
      path: 'document/crud-document/modify/:crudType/:documentId', 
      component: CrudDocumentComponent 
    },
    {
      path: 'ticket',
      component: TicketComponent,
    },
    {
      path: 'ticket/crud-ticket',
      component: CrudTicketComponent,
    },
    { 
      path: 'ticket/crud-ticket/modify/:crudType/:ticketId', 
      component: CrudTicketComponent 
    },
    
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ManagementRoutingModule {
}
