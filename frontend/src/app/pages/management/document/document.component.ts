import { Component, OnInit } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';

import { SmartTableService } from '../../../@core/data/smart-table.service';
import { Router, ActivatedRoute } from '@angular/router';
import { DbService } from '../../../services/db.service';
import { Logger } from '../../../services/Logger';


@Component({
  selector: 'ngx-document',
  templateUrl: './document.component.html',
  styleUrls: ['./document.component.scss']
})
export class DocumentComponent implements OnInit {


  ngOnInit() {
  }

  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    hideSubHeader: true,
    noDataMessage: "No Documents available",
    actions: {
      add: false,
      edit: false,
      delete: true
    },
    columns: {
      created_date: {
        title: 'Created Date',
        type: 'number',
      },
      status: {
        title: 'Status',
        type: 'string',
      },
      label: {
        title: 'label',
        type: 'string',
      },
      type: {
        title: 'Type',
        type: 'string',
      },
      confidentiality: {
        title: 'Confidentiality',
        type: 'string',
      },
      file_name: {
        title: 'Document Name',
        type: 'string',
      }
    },
  };

  source: LocalDataSource = new LocalDataSource();

  constructor(
    private router: Router,
    private service: SmartTableService,
    private _db: DbService,
    private activatedRoute: ActivatedRoute
  ) {

    this.getAllDocuments();
    // const data = this.getDataFromDb();
    // this.source.load(data);
  }


  onDeleteConfirm(event): void {
    Logger.logMsgObj("delete... ", event);
    Logger.logObj(event.data._id);
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
      Logger.logMsg("Will be deleted...");
      this._db.deleteDocument(event.data._id).subscribe(
        data => { 
          Logger.logMsg("Document deleted.");
        },
        err => { 
          Logger.logMsg("Error in document deletion.");
          Logger.logObj(err);
        }
      );
    } else {
      event.confirm.reject();
      Logger.logMsg("Will not be deleted...");
    }
  }

  getAllDocuments() {
    // this._db.getT();
    this._db.getAllDocuments().subscribe(
      data => {
        Logger.logMsgObj("Got Documentss from db: ", data);
        this.source.load(data["data"]);
      },
      error => {
        Logger.logMsgObj("error: ", error);
      }
    );
  }

  onAddDocumentClick() {
    console.log("Add Document will be called.");
    this.router.navigate(['/pages/management/document/crud-document']);
  }

  onDocumentRowSelect(event) {
    Logger.logMsg("Inside onDocumentRowSelect function");
    Logger.logObj(event);
    this.router.navigate(['/pages/management/document/crud-document/modify', "edit", event.data._id]);
  }
}
