import { Component, OnInit } from '@angular/core';
import { Logger } from '../../../../services/Logger';
import { DbService } from '../../../../services/db.service';
import { Document } from '../../../../models/Document.model';
import { Router, ActivatedRoute } from '@angular/router';
import { CustomMisc } from '../../../../services/CustomMisc';
import { BlockChainService } from '../../../../services/block-chain.service';

@Component({
  selector: 'ngx-crud-document',
  templateUrl: './crud-document.component.html',
  styleUrls: ['./crud-document.component.scss']
})
export class CrudDocumentComponent implements OnInit {

  document: Document;
  currentCrudType = "add";
  submitBtnText = "Submit";
  contactsArr = [];
  processOwner = "";
  defaultSpace = " ";
  isUploadFileImage = true;

  htmlLogoImageLink: string = "";
  currentLogoImageFile: File;
  uploadLogoImageFlag: boolean = false;

  constructor(private _db: DbService, private _blockchainService:BlockChainService, private router: Router, private _activatedRoute: ActivatedRoute) { }


  ngOnInit() {
    Logger.logMsg("Enter: CRUD Document Component");
    this.document = new Document();
    this.fillFormFields();
    this._activatedRoute.params.subscribe(
      params => {
        Logger.logMsgObj("Params:: ", params);
        //check if edit
        if (params["crudType"] == "edit") {
          this.currentCrudType = "edit";
          this.submitBtnText = "Update";
          let documentId = params["documentId"];
          this._db.getSingleDocument(documentId).subscribe(
            data => {
              Logger.logMsgObj("Got org: ", data["data"]);
              this.document = data["data"];
              this.htmlLogoImageLink = this.document.file_url;
            },
            error => {
              Logger.logMsgObj("Error:", error);
            }

          )
        }
      }
    );
  }


  fillFormFields() {
    this._db.getAllContacts().subscribe(
      data => {
        this.contactsArr = data["data"];
        Logger.logMsgObj("owner:: ", this.contactsArr);
      },
      error => {
        Logger.logMsg("Error: " + error);
      }
    );

  }

  async onSubmitClick() {
    Logger.logMsgObj("document tobe saved: ", this.document);
    this.document.status = "Active";//default

    if (this.currentLogoImageFile != null && this.currentLogoImageFile.name != this.document.file_name) {
      this.document.file_name = this.currentLogoImageFile.name;
      //upload files to server
      let tmpDocument = this.document;
      await CustomMisc.uploadFileToRemoteServer(this.currentLogoImageFile).then(function (dataLocation) {
        Logger.logMsg("datalocstr:" + dataLocation.toString());
        Logger.logMsgObj("document: ", tmpDocument);
        tmpDocument.file_url = dataLocation.toString();
      });
      this.document = tmpDocument;
    }


    if (this.currentCrudType == "add") {
      // this.document._id = null;
      this.document.created_date = this.getCurrentFormattedDate();
      this._db.saveNewDocument(this.document).subscribe(
        data => {
          Logger.logMsg(data);

          let documentId = data["data"]._id;
          //save to blockchain
          Logger.logMsg("Calling blockchain for documentId: " + documentId);
          this._blockchainService.addNewDocument(documentId).subscribe(
            data2 => {
              Logger.logMsgObj("reply from blockchain", data2);
            },
            err2 => {
              Logger.logMsgObj("error", err2);
            }
          );

          this.router.navigate(['/pages/management/document']);
        },
        err => {
          Logger.logMsg(err);
        }
      );
    } else {
      this._db.updateDocument(this.document).subscribe(
        data => {
          Logger.logMsg(data);
          this.router.navigate(['/pages/management/document']);
        },
        err => {
          Logger.logMsg(err);
        }
      );
    }
  }

  /**
  * 
  * @param event 
  */
  async onImageUpload(event) {
    let imageFile = event.target.files[0];
    Logger.logMsgObj("will upload the file::: ", imageFile);

    //preview the image
    var reader = new FileReader();
    reader.readAsDataURL(imageFile);
    reader.onload = (event: any) => {
      this.currentLogoImageFile = imageFile;
      this.htmlLogoImageLink = event.target.result;
      this.uploadLogoImageFlag = true;

    }
  }

  getCurrentFormattedDate() {
    let d = new Date(Date.now());
    let str = d.getDate() + "/" + (d.getMonth() + 1) + "/" + d.getFullYear();
    return str;
  }
}
