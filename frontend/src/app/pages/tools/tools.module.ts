import { NgModule } from '@angular/core';

import { ToasterModule } from 'angular2-toaster';
import { ThemeModule } from '../../@theme/theme.module';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ToolsComponent } from './tools.component';
import { ToolsRoutingModule } from './tools-routing.module';
import { ActivityComponent } from './activity/activity.component';
import { CrudActivityComponent } from './activity/crud-activity/crud-activity.component';
import { TomComponent } from './tom/tom.component';
import { CrudTomComponent } from './tom/crud-tom/crud-activity.component';
import { DatabreachComponent } from './databreach/databreach.component';
import { CrudDatabreachComponent } from './databreach/crud-databreach/crud-databreach.component';
import { DatasubjectrequestComponent } from './datasubjectrequest/datasubjectrequest.component';
import { CrudDatasubjectrequestComponent } from './datasubjectrequest/crud-datasubjectrequest/crud-datasubjectrequest.component';

const COMPONENTS = [
  ToolsComponent,
  ActivityComponent,
  CrudActivityComponent,
  TomComponent,
  CrudTomComponent,
  DatabreachComponent,
  CrudDatabreachComponent,
  DatasubjectrequestComponent,
  CrudDatasubjectrequestComponent
];

const SERVICES = [
  
];

const MODULES = [
  ThemeModule,
  ToolsRoutingModule,
  ToasterModule.forRoot(),
  Ng2SmartTableModule
];

@NgModule({
  imports: [
    ...MODULES,
  ],
  declarations: [
    ...COMPONENTS,
  ],
  providers: [
    ...SERVICES,
  ],
})
export class ToolsModule { }
