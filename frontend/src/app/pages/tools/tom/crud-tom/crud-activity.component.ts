import { Component, OnInit } from '@angular/core';
import { Logger } from '../../../../services/Logger';
import { DbService } from '../../../../services/db.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Activity } from '../../../../models/Activity.model';

@Component({
  selector: 'ngx-crud-activity',
  templateUrl: './crud-activity.component.html',
  styleUrls: ['./crud-activity.component.scss']
})
export class CrudTomComponent implements OnInit {

  activity: Activity;
  templatesArr = [];
  selectedTemplate: any;
  currentCrudType = "add";
  submitBtnText = "Submit";

  contactsArr = [];
  processOwner = "";
  defaultSpace = " ";

  constructor(private _db: DbService, private router: Router, private _activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    Logger.logMsg("Enter: CRUD Tom Component");
    this.activity = new Activity();
    this.fillActivityTemplates();
    this.fillFormFields();

    this._activatedRoute.params.subscribe(
      params => {
        Logger.logMsgObj("Params:: ", params);
        //check if edit
        if (params["crudType"] == "edit") {
          this.currentCrudType = "edit";
          this.submitBtnText = "Update";
          let activityId = params["activityId"];
          this._db.getSingleActivity(activityId).subscribe(
            data => {
              Logger.logMsgObj("Edit activity: ", data["data"]);
              this.activity = data["data"];
              this.processOwner = this.activity.process_owner;
              //get contact name from id
              // this._db.getSingleContact(this.activity.process_owner).subscribe(
              //   data1 => { 
              //     this.processOwner = data1["data"].first_name + " " + data1["data"].last_name;
              //     Logger.logMsg("processOwnerStr:: " + this.processOwner);
              //   },
              //   err => { 
              //     Logger.logMsgObj("Error:", err);
              //   }
              // );
            },
            error => {
              Logger.logMsgObj("Error:", error);
            }

          )
        }
      }
    );
  }

  fillFormFields() {
    this._db.getAllContacts().subscribe(
      data => {
        this.contactsArr = data["data"];
        Logger.logMsgObj("owner:: ", this.contactsArr);
      },
      error => {
        Logger.logMsg("Error: " + error);
      }
    );

  }

  getCurrentFormattedDate() {
    let d = new Date(Date.now());
    let str = d.getDate() + "/" + (d.getMonth() + 1) + "/" + d.getFullYear();
    return str;
  }

  onSubmitClick() {
    Logger.logMsgObj("activity to be saved: ", this.activity);
    this.activity.process_owner = this.processOwner;
    if (this.currentCrudType == "add") {
      // this.activity._id = null;
      this.activity.is_template = false;
      this.activity.created_date = this.getCurrentFormattedDate();

      this._db.saveNewActivity(this.activity).subscribe(
        data => {
          Logger.logMsg(data);
          this.router.navigate(['/pages/tools/activity']);
        },
        err => {
          Logger.logMsg(err);
        }
      );
    } else {
      this._db.updateActivity(this.activity).subscribe(
        data => {
          Logger.logMsg(data);
          this.router.navigate(['/pages/tools/activity']);
        },
        err => {
          Logger.logMsg(err);
        }
      );
    }

  }

  fillActivityTemplates() {
    this._db.getAllActivityTemplates().subscribe(
      data => {
        Logger.logMsgObj("Got reply from db: ", data);
        this.templatesArr = data["data"];
      },
      error => {
        Logger.logMsgObj("error: ", error);
      }
    );
  }

  onSelectTemplate(event) {
    Logger.logMsg("selected template: ");
    Logger.logObj(this.selectedTemplate);
    // this.activity = new Activity(this.selectedTemplate);
    delete this.selectedTemplate._id;
    this.copyTemplateActivity(this.selectedTemplate);
  }

  copyTemplateActivity(templateActivity: Activity) {
    this.activity.data_deletion_process = templateActivity.data_deletion_process;
    this.activity.department = templateActivity.department;
    this.activity.label = templateActivity.label;
    this.activity.process_owner = templateActivity.process_owner;
    this.activity.purpose = templateActivity.purpose;
    this.activity.role = templateActivity.role;
    this.activity.status = templateActivity.status;
    this.activity.storage_period = templateActivity.storage_period;
    this.activity.is_legal_public_interest = templateActivity.is_legal_public_interest;
    this.activity.legal_text_public_interest = templateActivity.legal_text_public_interest;
    this.activity.is_legal_consent = templateActivity.is_legal_consent;
    this.activity.legal_text_consent = templateActivity.legal_text_consent;
    this.activity.is_legal_contract = templateActivity.is_legal_contract;
    this.activity.legal_text_contract = templateActivity.legal_text_contract;
    this.activity.is_access_ceo = templateActivity.is_access_ceo;
    this.activity.is_access_ciso = templateActivity.is_access_ciso;
    this.activity.is_access_counsel = templateActivity.is_access_counsel;
    this.activity.is_toms_ability_it = templateActivity.is_toms_ability_it;
    this.activity.is_toms_alarm = templateActivity.is_toms_alarm;
    this.activity.is_toms_automatic_logging = templateActivity.is_toms_automatic_logging;
  }

  onResetClick() {
    this.activity = new Activity(false);
  }

  onCancelClick(){
    this.router.navigate(['/pages/tools/tom']);    
  }
}
