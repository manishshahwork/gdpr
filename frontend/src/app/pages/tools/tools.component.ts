import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-components',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class ToolsComponent implements OnInit {
  ngOnInit(){
    console.log("Enter: Tools Component");
  }
}
