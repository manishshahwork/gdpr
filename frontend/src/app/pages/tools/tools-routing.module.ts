import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ToolsComponent } from './tools.component';
import { ActivityComponent } from './activity/activity.component';
import { CrudActivityComponent } from './activity/crud-activity/crud-activity.component';
import { TomComponent } from './tom/tom.component';
import { CrudTomComponent } from './tom/crud-tom/crud-activity.component';
import { DatabreachComponent } from './databreach/databreach.component';
import { CrudDatabreachComponent } from './databreach/crud-databreach/crud-databreach.component';
import { DatasubjectrequestComponent } from './datasubjectrequest/datasubjectrequest.component';
import { CrudDatasubjectrequestComponent } from './datasubjectrequest/crud-datasubjectrequest/crud-datasubjectrequest.component';


const routes: Routes = [{
  path: '',
  component: ToolsComponent,

  children: [
    {
      path: 'activity',
      component: ActivityComponent,
    },
    {
      path: 'activity/crud-activity',
      component: CrudActivityComponent,
    },    
    {
      path: 'activity/crud-activity/modify/:crudType/:activityId',
      component: CrudActivityComponent
    },

    {
      path: 'tom',
      component: TomComponent,
    },
    {
      path: 'tom/crud-tom',
      component: CrudTomComponent,
    },    
    {
      path: 'tom/crud-tom/modify/:crudType/:activityId',
      component: CrudTomComponent
    },

    {
      path: 'databreach',
      component: DatabreachComponent,
    },
    {
      path: 'databreach/crud-databreach',
      component: CrudDatabreachComponent,
    },    
    {
      path: 'databreach/crud-databreach/modify/:crudType/:activityId',
      component: CrudDatabreachComponent
    },


    {
      path: 'datasubjectrequest',
      component: DatasubjectrequestComponent,
    },
    {
      path: 'datasubjectrequest/crud-datasubjectrequest',
      component: CrudDatasubjectrequestComponent,
    },    
    {
      path: 'datasubjectrequest/crud-datasubjectrequest/modify/:crudType/:activityId',
      component: CrudDatasubjectrequestComponent
    },



  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ToolsRoutingModule {
}
