import { delay } from 'rxjs/operators';
import { AfterViewInit, Component, Input, OnDestroy } from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { DbService } from '../../../services/db.service';
import { Logger } from '../../../services/Logger';
import { Router } from '@angular/router';


declare const echarts: any;

@Component({
  selector: 'ngx-solar',
  styleUrls: ['./solar.component.scss'],
  templateUrl: './solar.component.html',
})
export class SolarComponent implements AfterViewInit, OnDestroy {

  totalUsers = 1;
  totalContacts = 1;
  totalActivities = 1;
  totalDocuments = 1;

  private value = 50;
  currentChartType = 1;
  displayChartType = "Something";

  @Input('chartType')
  set chartType(v: number) {
    console.log("v:: :" + v);
    if(v == 1){
      this.currentChartType = 1;
      this.displayChartType = "Users";
      this.fetchTotalUsers();
    } else if(v == 2){
      this.currentChartType = 2;
      this.displayChartType = "Contacts";
      this.fetchTotalContacts();
    } else if(v == 3){
      this.currentChartType = 3;
      this.displayChartType = "Activities";
      this.fetchTotalActivities();
    }  else if(v == 4){
      this.currentChartType = 4;
      this.displayChartType = "Documents";
      this.fetchTotalDocuments();
    } 
    
    // // this.value = value;
    // if (this.option.series) {
    //   this.option.series[0].data[0].value = value;
    //   this.option.series[0].data[1].value = 100 - value;
    //   this.option.series[1].data[0].value = value;
    // }
  }

  onViewClick(chartType){
    console.log("will direct to : " + this.currentChartType);
    if(chartType == 1){
      this.router.navigate(['/pages/management/user']);
    } else if(chartType == 2){
      this.router.navigate(['/pages/management/contact']);
    } else if(chartType == 3){
      this.router.navigate(['/pages/tools/activity']);
    }else if(chartType == 4){
      this.router.navigate(['/pages/management/document']);
    } 
  }

  fetchTotalActivities(){
    this._db.getAllActivities().subscribe(
      data =>{
        this.value = data["data"].length;
        this.totalActivities = data["data"].length;
      },
      err => {
        Logger.logMsgObj("Error: " , err);
      }
    )
  }

  fetchTotalUsers(){
    this._db.getAllUsers().subscribe(
      data =>{
        Logger.logMsgObj("Total users: ", data);
        Logger.logMsg("count: " + data["data"].length);
        this.value = data["data"].length;
        this.totalUsers = data["data"].length;
      },
      err => {
        Logger.logMsgObj("Error: " , err);
      }
    )
  }

  fetchTotalContacts(){
    this._db.getAllContacts().subscribe(
      data =>{
        Logger.logMsgObj("Total contacts: ", data);
        Logger.logMsg("count: " + data["data"].length);
        this.value = data["data"].length;
        this.totalContacts = data["data"].length;
      },
      err => {
        Logger.logMsgObj("Error: " , err);
      }
    )
  }


  fetchTotalDocuments(){
    this._db.getAllDocuments().subscribe(
      data =>{
        Logger.logMsgObj("Total documents: ", data);
        Logger.logMsg("count: " + data["data"].length);
        this.value = data["data"].length;
        this.totalDocuments = data["data"].length;
      },
      err => {
        Logger.logMsgObj("Error: " , err);
      }
    )
  }

  option: any = {};
  themeSubscription: any;

  constructor(private theme: NbThemeService, private _db:DbService, private router:Router) {
    
    console.log("About to fetch chart type: " + this.displayChartType);
    this.fetchTotalUsers();
    this.fetchTotalContacts();
    this.fetchTotalActivities();
    this.fetchTotalDocuments();
    this.drawChart();
  }

  ngAfterViewInit() {
    this.themeSubscription = this.theme.getJsTheme().pipe(delay(1)).subscribe(config => {

      const solarTheme: any = config.variables.solar;

      this.option = Object.assign({}, {
        tooltip: {
          trigger: 'item',
          formatter: '{a} <br/>{b} : {c} ({d}%)',
        },
        series: [
          {
            name: ' ',
            clockWise: true,
            hoverAnimation: false,
            type: 'pie',
            center: ['45%', '50%'],
            radius: solarTheme.radius,
            data: [
              {
                value: this.value,
                name: ' ',
                label: {
                  normal: {
                    position: 'center',
                    formatter: '{d}',
                    textStyle: {
                      fontSize: '22',
                      fontFamily: config.variables.fontSecondary,
                      fontWeight: '600',
                      color: config.variables.fgHeading,
                    },
                  },
                },
                tooltip: {
                  show: false,
                },
                itemStyle: {
                  normal: {
                    color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
                      {
                        offset: 0,
                        color: solarTheme.gradientLeft,
                      },
                      {
                        offset: 1,
                        color: solarTheme.gradientRight,
                      },
                    ]),
                    shadowColor: solarTheme.shadowColor,
                    shadowBlur: 0,
                    shadowOffsetX: 0,
                    shadowOffsetY: 3,
                  },
                },
                hoverAnimation: false,
              },
              {
                value: 100 - this.value,
                name: ' ',
                tooltip: {
                  show: false,
                },
                label: {
                  normal: {
                    position: 'inner',
                  },
                },
                itemStyle: {
                  normal: {
                    color: config.variables.layoutBg,
                  },
                },
              },
            ],
          },
          {
            name: ' ',
            clockWise: true,
            hoverAnimation: false,
            type: 'pie',
            center: ['45%', '50%'],
            radius: solarTheme.radius,
            data: [
              {
                value: this.value,
                name: ' ',
                label: {
                  normal: {
                    position: 'inner',
                    show: false,
                  },
                },
                tooltip: {
                  show: false,
                },
                itemStyle: {
                  normal: {
                    color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
                      {
                        offset: 0,
                        color: solarTheme.gradientLeft,
                      },
                      {
                        offset: 1,
                        color: solarTheme.gradientRight,
                      },
                    ]),
                    shadowColor: solarTheme.shadowColor,
                    shadowBlur: 7,
                  },
                },
                hoverAnimation: false,
              },
              {
                value: 28,
                name: ' ',
                tooltip: {
                  show: false,
                },
                label: {
                  normal: {
                    position: 'inner',
                  },
                },
                itemStyle: {
                  normal: {
                    color: 'none',
                  },
                },
              },
            ],
          },
        ],
      });
    });
  }

  ngOnDestroy() {
    this.themeSubscription.unsubscribe();
  }


  
  userChartData: any;
  userChartOptions: any;
  contactChartData: any;
  contactChartOptions: any;
  activityChartData: any;
  activityChartOptions: any;
  documentChartData: any;
  documentChartOptions: any;

  drawChart(){
    this.drawUserChart();
    this.drawContactChart();
    this.drawActivityChart();
    this.drawDocumentChart();
  }
  
  drawUserChart() {
    this.themeSubscription = this.theme.getJsTheme().subscribe(config => {
      const colors: any = config.variables;
      const chartjs: any = config.variables.chartjs;

      this.userChartData = {
        labels: ['Users'],
        datasets: [{
          data: [this.totalUsers],
          backgroundColor: [colors.primaryLight],
        }],
      };
      this.userChartOptions = {
        maintainAspectRatio: false,
        responsive: true,
        scales: {
          xAxes: [
            {
              display: false,
            },
          ],
          yAxes: [
            {
              display: false,
            },
          ],
        },
        legend: {
          display:false,
          labels: {
            fontColor: chartjs.textColor,
          },
        },
      };
    });
  }

  drawContactChart() {
    this.themeSubscription = this.theme.getJsTheme().subscribe(config => {
      const colors: any = config.variables;
      const chartjs: any = config.variables.chartjs;

      this.contactChartData = {
        labels: ['Contacts'],
        datasets: [{
          data: [this.totalContacts],
          backgroundColor: [colors.successLight],
        }],
      };
      this.contactChartOptions = {
        maintainAspectRatio: false,
        responsive: true,
        scales: {
          xAxes: [
            {
              display: false,
            },
          ],
          yAxes: [
            {
              display: false,
            },
          ],
        },
        legend: {
          display:false,
          labels: {
            fontColor: chartjs.textColor,
          },
        },
      };
    });
  }

  drawActivityChart() {
    this.themeSubscription = this.theme.getJsTheme().subscribe(config => {
      const colors: any = config.variables;
      const chartjs: any = config.variables.chartjs;

      this.activityChartData = {
        labels: ['Activities'],
        datasets: [{
          data: [this.totalActivities],
          backgroundColor: [colors.infoLight],
        }],
      };
      this.activityChartOptions = {
        maintainAspectRatio: false,
        responsive: true,
        scales: {
          xAxes: [
            {
              display: false,
            },
          ],
          yAxes: [
            {
              display: false,
            },
          ],
        },
        legend: {
          display:false,
          labels: {
            fontColor: chartjs.textColor,
          },
        },
      };
    });
  }

  drawDocumentChart(){
    this.themeSubscription = this.theme.getJsTheme().subscribe(config => {
      const colors: any = config.variables;
      const chartjs: any = config.variables.chartjs;

      this.documentChartData = {
        labels: ['Documents'],
        datasets: [{
          data: [this.totalDocuments],
          backgroundColor: [colors.successLight],
        }],
      };
      this.documentChartOptions = {
        maintainAspectRatio: false,
        responsive: true,
        scales: {
          xAxes: [
            {
              display: false,
            },
          ],
          yAxes: [
            {
              display: false,
            },
          ],
        },
        legend: {
          display:false,
          labels: {
            fontColor: chartjs.textColor,
          },
        },
      };
    });
  }

}
