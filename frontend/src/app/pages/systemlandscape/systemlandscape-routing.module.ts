import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SystemlandscapeComponent } from './systemlandscape.component';
import { SystemComponent } from './system/system.component';
import { CrudSystemComponent } from './system/crud-system/crud-system.component';



const routes: Routes = [{
  path: '',
  component: SystemlandscapeComponent,

  children: [
    {
      path: 'system',
      component: SystemComponent,
    },
    {
      path: 'system/crud-system',
      component: CrudSystemComponent,
    },
    { 
      path: 'system/crud-system/modify/:crudType/:documentId', 
      component: CrudSystemComponent
    },
    
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SystemlandscapeRoutingModule {
}
