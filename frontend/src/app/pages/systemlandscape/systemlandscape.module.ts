import { NgModule } from '@angular/core';

import { ToasterModule } from 'angular2-toaster';
import { ThemeModule } from '../../@theme/theme.module';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { SystemlandscapeComponent } from './systemlandscape.component';
import { SystemlandscapeRoutingModule } from './systemlandscape-routing.module';
import { SystemComponent } from './system/system.component';
import { CrudSystemComponent } from './system/crud-system/crud-system.component';


const COMPONENTS = [
  SystemlandscapeComponent,
  SystemComponent,
  CrudSystemComponent
];

const SERVICES = [
  
];

const MODULES = [
  ThemeModule,
  SystemlandscapeRoutingModule,
  ToasterModule.forRoot(),
  Ng2SmartTableModule
];

@NgModule({
  imports: [
    ...MODULES,
  ],
  declarations: [
    ...COMPONENTS,
  ],
  providers: [
    ...SERVICES,
  ],
})
export class SystemlandscapeModule { }
