import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-components',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class SystemlandscapeComponent implements OnInit {
  ngOnInit(){
    console.log("Enter: System Landscape Component");
  }
}



