import { Injectable } from '@angular/core';
import { Logger } from './Logger';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BlockChainService {

  private BLOCK_CHAIN_URL = "http://ec2-13-232-190-139.ap-south-1.compute.amazonaws.com:3000/api/"
  constructor(private http: HttpClient) { }

  addNewContact(contactId) {
    Logger.logMsg("New contact with id:'" + contactId + "' will be added to block chain");
    let url = this.BLOCK_CHAIN_URL + "Contact";
    let obj = {
      "$class": "net.biz.digitalPropertyNetwork.Contact",
      "contactId": contactId,
      "contactHex": contactId
    }
    Logger.logMsg("Blockchain URL: " + url);
    Logger.logMsgObj("obj", obj);
    return this.http.post(
      url,
      obj);

  }

  addNewUser(userId) {
    Logger.logMsg("New user with id:'" + userId + "' will be added to block chain");
    let url = this.BLOCK_CHAIN_URL + "User";
    let obj = {
      "$class": "net.biz.digitalPropertyNetwork.User",
      "userId": userId,
      "userHex": userId
    }
    Logger.logMsgObj("obj", obj);
    return this.http.post(
      url,
      obj);

  }

  addNewActivity(activityId) {
    Logger.logMsg("New activity with id:'" + activityId + "' will be added to block chain");
    let url = this.BLOCK_CHAIN_URL + "Activity";
    let obj = {
      "$class": "net.biz.digitalPropertyNetwork.Activity",
      "activityId": activityId,
      "activityHex": activityId
    }
    Logger.logMsgObj("obj", obj);
    return this.http.post(
      url,
      obj);

  }

  addNewOrganization(organizationId) {
    Logger.logMsg("New organization with id:'" + organizationId + "' will be added to block chain");
    let url = this.BLOCK_CHAIN_URL + "Organisation";
    let obj = {
      "$class": "net.biz.digitalPropertyNetwork.Organisation",
      "organisationId": organizationId,
      "organisationHex": organizationId
    }
    Logger.logMsgObj("obj", obj);
    return this.http.post(
      url,
      obj);

  }

  addNewDocument(documentId) {
    Logger.logMsg("New document with id:'" + documentId + "' will be added to block chain");
    let url = this.BLOCK_CHAIN_URL + "Document";
    let obj = {
      "$class": "net.biz.digitalPropertyNetwork.Document",
      "documentId": documentId,
      "documentHex": documentId
    }
    Logger.logMsgObj("obj", obj);
    return this.http.post(
      url,
      obj);

  }

}
