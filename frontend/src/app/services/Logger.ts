export class Logger {
    static logMsg(msg){
        console.log(msg);
    }

    static logObj(obj){
        console.log(obj);
    }

    static logMsgObj(msg, obj){
        this.logMsg(msg);
        this.logObj(obj);
    }
}