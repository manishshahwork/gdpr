import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Organization } from '../models/Organization.model';
import { Contact } from '../models/Contact.model';
import { User } from '../models/User.model';
import { Activity } from '../models/Activity.model';
import { Document } from '../models/Document.model';
import { Logger } from './Logger';

@Injectable({
  providedIn: 'root'
})
export class DbService {
  HOST_URL = "http://localhost:3000";
  constructor(private http: HttpClient) { }

  ////
  saveNewOrganization(organization: Organization) {
    return this.http.post(
      this.HOST_URL + "/management/organization/addOrganization",
      organization);
  }

  updateOrganization(organization: Organization) {
    return this.http.post(
      this.HOST_URL + "/management/organization/updateOrganization",
      organization);
  }

  getAllOrganizations() {
    return this.http.get(this.HOST_URL + "/management/organization/getAllOrganizations");
  }

  getAllOrganizationTemplates() {
    return this.http.get(this.HOST_URL + "/management/organization/getAllTemplates");
  }

  getSingleOrganization(organizationId) {
    let url = this.HOST_URL + "/management/organization/getSingleOrganization/" + organizationId;
    return this.http.get(url);
  }

  ////
  saveNewContact(contact: Contact) {
    return this.http.post(
      this.HOST_URL + "/management/contact/addContact",
      contact);
  }
  getAllContacts() {
    return this.http.get(this.HOST_URL + "/management/contact/getAllContacts");
  }

  updateContact(contact: Contact) {
    return this.http.post(
      this.HOST_URL + "/management/contact/updateContact",
      contact);
  }

  getSingleContact(contactId) {
    let url = this.HOST_URL + "/management/contact/getSingleContact/" + contactId;
    return this.http.get(url);
  }
  ////
  saveNewUser(user: User) {
    return this.http.post(
      this.HOST_URL + "/management/user/addUser",
      user);
  }
  getAllUsers() {
    return this.http.get(this.HOST_URL + "/management/user/getAllUsers");
  }
  // getTotalUsers(){
  //   return this.http.get(this.HOST_URL + "/management/user/getTotalUsers");
  // }
  updateUser(user: User) {
    return this.http.post(
      this.HOST_URL + "/management/user/updateUser",
      user);
  }

  getSingleUser(userId) {
    let url = this.HOST_URL + "/management/user/getSingleUser/" + userId;
    return this.http.get(url);
  }
  ////
  saveNewActivity(activity: Activity) {
    return this.http.post(
      this.HOST_URL + "/management/activity/addActivity",
      activity);
  }
  getAllActivities() {
    return this.http.get(this.HOST_URL + "/management/activity/getAllActivities");
  }

  getSingleActivity(activityId) {
    let url = this.HOST_URL + "/management/activity/getSingleActivity/" + activityId;
    return this.http.get(url);
  }

  updateActivity(activity: Activity) {
    return this.http.post(
      this.HOST_URL + "/management/activity/updateActivity",
      activity);
  }

  getAllActivityTemplates() {
    return this.http.get(this.HOST_URL + "/management/activity/getAllTemplates");
  }

  ////
  getAllDocuments() {
    return this.http.get(this.HOST_URL + "/management/document/getAllDocuments");
  }
  saveNewDocument(document: Document) {
    return this.http.post(
      this.HOST_URL + "/management/document/addDocument",
      document);
  }
  updateDocument(document: Document) {
    return this.http.post(
      this.HOST_URL + "/management/document/updateDocument",
      document);
  }

  getSingleDocument(documentId) {
    let url = this.HOST_URL + "/management/document/getSingleDocument/" + documentId;
    return this.http.get(url);
  }

  deleteDocument(documentId) {
    Logger.logMsg("documentId:: " + documentId);
    return this.http.post(this.HOST_URL + "/management/document/deleteDocument/"+documentId, documentId);
  }
}