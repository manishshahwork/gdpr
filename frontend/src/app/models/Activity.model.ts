export class Activity {
    // _id: string;
    status: string;
    label: string;
    purpose: string;
    role: string;
    department: string;
    process_owner: string;
    storage_period: string;

    last_impact_assessment_date: number;
    data_deletion_process: string;
    data_portability_process: string;
    data_minimization_process: string;
    created_date: string;

    //legal
    is_legal_public_interest: boolean = false;
    legal_text_public_interest: string;
    is_legal_consent: boolean = false;
    legal_text_consent: string;
    is_legal_contract: boolean = false;
    legal_text_contract: string;

    //access
    is_access_ceo: boolean = false;
    is_access_ciso: boolean = false;
    is_access_counsel: boolean = false;

    //toms
    is_toms_ability_it: boolean = false;
    is_toms_alarm: boolean = false;
    is_toms_automatic_logging: boolean = false;

    is_template: boolean = false;

    constructor(is_template = false) {
        this.is_template = is_template;
    }
}