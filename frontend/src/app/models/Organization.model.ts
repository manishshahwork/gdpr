export class Organization {
    // _id: string="";
    name: string = "";
    street: string = "";
    zip: string = "";
    city: string = "";
    phone: string = "";
    country: string = "";
    email: string = "";
    icon: string = "";
    website: string = "";
    is_template: boolean = false;

    constructor(is_template=false){
        this.is_template = is_template;
    }
}