export class Document {
    // _id: string = "";
    created_date: string = "";
    label: string = "";
    file_name: string = "";
    file_url: string = "";
    details: string = "";
    status: string = "";
    document_date: number = -1;
    resubmission: number = -1;
    type: string = "";
    confidentiality: string = "";
    iso_reference: string = "";
    permissions: string = "";
}