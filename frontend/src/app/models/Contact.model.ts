export class Contact {
    // _id: string = "";
    salutation: string = "";
    first_name: string = "";
    last_name: string = "";
    email: string = "";
    role: string = "";
    phone: string = "";
    organization_id: string = "";
}