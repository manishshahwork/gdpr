const express = require("express");
const router = express.Router();
const Operations = require("../operations");
const Organization = require("../models/Organization");
const Contact = require("../models/Contact");
const User = require("../models/User");
const Activity = require("../models/Activity");
const Logger = require("../config/Logger");
const Misc = require("../config/Misc");
const Document = require("../models/Document");

///////////////////////////// class - start

///////////////////////////// organization - start
//get organization by id
router.get("/organization/getById/:id", async function (req, res) {
    console.log("11111111111111111");
    Logger.logMsg("getById");
    Logger.logObj(req.params);
    let id = req.params.id;
    try {
        if (id == null) throw error("ID invalid");
        Organization.findById(id, function (err, result) {
            if (err) Misc.respondCommonError(res, err.message);
            else Misc.respondCommonSuccess(res, "success in getting by id", data);
        })
    } catch (err) {
        Misc.returnCommonError(res, "Error in getting by id", err.message);
    }
});

//add organization
router.post("/organization/addOrganization", function (req, res) {
    Logger.logMsg("Organization to be added ");
    Logger.logObj(req.body);
    Logger.logObj(req.params);
    let newOrganization = new Organization(req.body);
    Logger.logMsg("About to save organization:");
    Logger.logObj(newOrganization);
    try {
        Operations.addOrganization(newOrganization).then(reply => {
            Misc.respondCommonSuccess(res, "Organization added successfully", reply);
        });
    } catch (err) {
        Misc.respondCommonError(res, "Error in adding Organization");
    }
});

//update organization
router.post("/organization/updateOrganization", function (req, res) {
    Logger.logMsg("Organization to be updated ");
    Logger.logObj(req.body);
    Logger.logObj(req.params);
    let newOrganization = new Organization(req.body);
    Logger.logMsg("About to update organization:");
    Logger.logObj(newOrganization);
    Organization.update({ _id: req.body._id }, req.body)
        .then(
            doc => {
                Logger.logMsgObj("doc::: ", doc);
                if (doc) Misc.respondCommonSuccess(res, "Success in updating Organization", doc);
                else Misc.respondCommonError(res, "Error in updating Organization");
            }
        )
        .catch(
            err => {
                console.log(err);
                Misc.respondCommonError(res, "Error in updating Organization");
            }
        )
});

//get single organization 
router.get("/organization/getSingleOrganization/:organizationId", async function (req, res) {
    Logger.logMsg("Enter:getSingleOrganization()");
    Logger.logObj(req.body);
    Logger.logObj(req.params);
    let organizationId = req.params.organizationId
    try {
        let organization = await Organization.findById(organizationId);
        Logger.logMsgObj("got organization: ", organization);
        Misc.respondCommonSuccess(res, "success in getting organization", organization);
    } catch (err) {
        Misc.respondCommonError(res, "Error in getting organization", err.message);
    }
});

//get all organization
router.get("/organization/getAllOrganizations", async function (req, res) {
    Logger.logMsg("Enter:getAllOrganizations()");
    try {
        let templatesArr = await Operations.getAllOrganizations(false);
        Logger.logMsgObj("got templates: ", templatesArr);
        Misc.respondCommonSuccess(res, "success in getting all templates", templatesArr);
    } catch (err) {
        Misc.respondCommonError(res, "Error in getting templates");
    }
});

//get all organization templates
router.get("/organization/getAllTemplates", async function (req, res) {
    Logger.logMsg("Enter:getAllTemplates()");
    try {
        let templatesArr = await Operations.getAllOrganizations(true);
        Logger.logMsgObj("got templates: ", templatesArr);
        Misc.respondCommonSuccess(res, "success in getting all templates", templatesArr);
    } catch (err) {
        Misc.respondCommonError(res, "Error in getting templates");
    }
});
///////////////////////////// organization - end

///////////////////////////// contact - start
//get all contacts 
router.get("/contact/getAllContacts", async function (req, res) {
    Logger.logMsg("getAllContacts");
    try {
        let contactsArr = await Contact.find();
        Logger.logMsgObj("Contacts arr: ", contactsArr);
        Misc.respondCommonSuccess(res, null, contactsArr);

    } catch (err) {
        Misc.respondCommonError(res, err.message);
    }
});

//add contact
router.post("/contact/addContact", async function (req, res) {
    Logger.logMsg("Contact to be added ");
    Logger.logObj(req.body);
    Logger.logObj(req.params);
    // await Contact.findOneAndUpdate(req.body, function(err1, doc1){
    //     if(err1){
    //         console.log("eeeeeeeeeee");
    //         console.log(err1);
    //     } else {
    //         console.log("dddddddddddd");
    //         console.log(doc1);
    //     }
    // })
    // Misc.respondCommonSuccess(res, "Contact added successfully");
    
    let newContact = new Contact(req.body);
    Logger.logMsg("About to save contact:");
    Logger.logObj(newContact);
    try {        
        newContact.save(function(err, data){
            if(err){
                Logger.logMsgObj("error in adding contact", err);
                Misc.respondCommonError(res, "Error in adding Contact", err);
            } else {
                Logger.logMsgObj("Got contact", data);
                Misc.respondCommonSuccess(res, "Contact added successfully", data);
            }
        })

    } catch (err) {
        Misc.respondCommonError(res, "Error in adding Contact");
    }
});

//get all contacts for the given organization
router.get("/contact/getAllContactForOrganization/:organizationId", async function (req, res) {
    Logger.logMsg("getAllContactForOrganization");
    Logger.logObj(req.params);
    Logger.logObj(req.body);
    let organizationId = req.params.organizationId;
    try {
        let contactsArr = await Operations.getMatchingContacts(organizationId);
        Logger.logMsgObj("Contacts arr: ", contactsArr);
        Misc.respondCommonSuccess(res, null, contactsArr);

    } catch (err) {
        Misc.respondCommonError(res, err.message);
    }
});

//update contact
router.post("/contact/updateContact", function (req, res) {
    Logger.logMsg("Contact to be updated ");
    Logger.logObj(req.body);
    Logger.logObj(req.params);
    let newContact = new Contact(req.body);
    Logger.logMsg("About to update Contact:");
    Logger.logObj(newContact);
    Contact.update({ _id: req.body._id }, req.body)
        .then(
            doc => {
                Logger.logMsgObj("doc::: ", doc);
                if (doc) Misc.respondCommonSuccess(res, "Success in updating Contact", doc);
                else Misc.respondCommonError(res, "Error in updating Contact");
            }
        )
        .catch(
            err => {
                console.log(err);
                Misc.respondCommonError(res, "Error in updating Contact");
            }
        )
});

//get single contact 
router.get("/contact/getSingleContact/:contactId", async function (req, res) {
    Logger.logMsg("Enter:getSingleContact()");
    Logger.logObj(req.body);
    Logger.logObj(req.params);
    let contactId = req.params.contactId
    try {
        let contact = await Contact.findById(contactId);
        Logger.logMsgObj("got contact: ", contact);
        Misc.respondCommonSuccess(res, "success in getting contact", contact);
    } catch (err) {
        Misc.respondCommonError(res, "Error in getting contact", err.message);
    }
});

///////////////////////////// contact - end

///////////////////////////// user - start
//add user
router.post("/user/addUser", function (req, res) {
    Logger.logMsg("User to be added ");
    Logger.logObj(req.body);
    let newUser = new User(req.body);
    Logger.logMsg("About to save User:");
    Logger.logObj(newUser);
    try {
        Operations.addUser(newUser).then(reply => {
            Misc.respondCommonSuccess(res, "User added successfully", reply);
        });
    } catch (err) {
        Misc.respondCommonError(res, "Error in adding User");
    }
});

//get all users for the given user
router.get("/user/getAllUsers", async function (req, res) {
    Logger.logMsg("getAllUsers");
    let userId = req.params.userId;
    try {
        let usersArr = await User.find();
        Logger.logMsgObj("Users arr: ", usersArr);
        Misc.respondCommonSuccess(res, null, usersArr);

    } catch (err) {
        Misc.respondCommonError(res, err.message);
    }
});

//update User
router.post("/user/updateUser", function (req, res) {
    Logger.logMsg("User to be updated ");
    Logger.logObj(req.body);
    Logger.logObj(req.params);
    let newUser = new User(req.body);
    Logger.logMsg("About to update User:");
    Logger.logObj(newUser);
    User.update({ _id: req.body._id }, req.body)
        .then(
            doc => {
                Logger.logMsgObj("doc::: ", doc);
                if (doc) Misc.respondCommonSuccess(res, "Success in updating User", doc);
                else Misc.respondCommonError(res, "Error in updating User");
            }
        )
        .catch(
            err => {
                console.log(err);
                Misc.respondCommonError(res, "Error in updating User");
            }
        )
});

//get single user 
router.get("/user/getSingleUser/:userId", async function (req, res) {
    Logger.logMsg("Enter:getSingleUser()");
    Logger.logObj(req.body);
    Logger.logObj(req.params);
    let userId = req.params.userId
    try {
        let user = await User.findById(userId);
        Logger.logMsgObj("got user: ", user);
        Misc.respondCommonSuccess(res, "success in getting user", user);
    } catch (err) {
        Misc.respondCommonError(res, "Error in getting user", err.message);
    }
});
///////////////////////////// user - end


///////////////////////////// activity - start
//get all Activities for the given activity
router.get("/activity/getAllActivities", async function (req, res) {
    Logger.logMsg("getAllActivities");
    try {
        let templatesArr = await Operations.getAllActivities(false);
        Logger.logMsgObj("Activities template arr: ", templatesArr);
        Misc.respondCommonSuccess(res, null, templatesArr);

    } catch (err) {
        Misc.respondCommonError(res, err.message);
    }
});

//add activity
router.post("/activity/addActivity", function (req, res) {
    Logger.logMsg("Activity to be added ");
    Logger.logObj(req.body);
    let newActivity = new Activity(req.body);
    Logger.logMsg("About to save Activity:");
    Logger.logObj(newActivity);
    try {
        Operations.addActivity(newActivity).then(reply => {
            Misc.respondCommonSuccess(res, "Activity added successfully", reply);
        });
    } catch (err) {
        Misc.respondCommonError(res, "Error in adding Activity");
    }
});

//get all activity templates
router.get("/activity/getAllTemplates", async function (req, res) {
    Logger.logMsg("Enter:getAllTemplates()");
    try {
        let templatesArr = await Operations.getAllActivities(true);
        Logger.logMsgObj("got templates: ", templatesArr);
        Misc.respondCommonSuccess(res, "success in getting all templates", templatesArr);
    } catch (err) {
        Misc.respondCommonError(res, "Error in getting templates");
    }
});

//get single activity 
router.get("/activity/getSingleActivity/:activityId", async function (req, res) {
    Logger.logMsg("Enter:getSingleActivity()");
    Logger.logObj(req.body);
    Logger.logObj(req.params);
    let activityId = req.params.activityId
    try {
        let activity = await Activity.findById(activityId);
        Logger.logMsgObj("got activity: ", activity);
        Misc.respondCommonSuccess(res, "success in getting activity", activity);
    } catch (err) {
        Misc.respondCommonError(res, "Error in getting activity", err.message);
    }
});

//update activity
router.post("/activity/updateActivity", function (req, res) {
    Logger.logMsg("activity to be updated ");
    Logger.logObj(req.body);
    Logger.logObj(req.params);
    let newActivity = new Activity(req.body);
    Logger.logMsg("About to update Activity:");
    Logger.logObj(newActivity);
    Activity.update({ _id: req.body._id }, req.body)
        .then(
            doc => {
                Logger.logMsgObj("doc::: ", doc);
                if (doc) Misc.respondCommonSuccess(res, "Success in updating Activity", doc);
                else Misc.respondCommonError(res, "Error in adding Activity");
            }
        )
        .catch(
            err => {
                console.log(err);
                Misc.respondCommonError(res, "Error in adding Activity");
            }
        )
});
///////////////////////////// activity - end




///////////////////////////// document - start
//add document
router.post("/document/addDocument", function (req, res) {
    Logger.logMsg("Document to be added ");
    Logger.logObj(req.body);
    let newDocument = new Document(req.body);
    Logger.logMsg("About to save Document:");
    Logger.logObj(newDocument);
    try {
        Operations.addDocument(newDocument).then(reply => {
            Misc.respondCommonSuccess(res, "Document added successfully", reply);
        });
    } catch (err) {
        Logger.logMsgObj(err);
        Misc.respondCommonError(res, "Error in adding Document");
    }
});

//get all documents for the given document
router.get("/document/getAllDocuments", async function (req, res) {
    Logger.logMsg("getAllDocuments");
    let documentId = req.params.documentId;
    try {
        let documentsArr = await Document.find();
        Logger.logMsgObj("Documents arr: ", documentsArr);
        Misc.respondCommonSuccess(res, null, documentsArr);

    } catch (err) {
        Misc.respondCommonError(res, err.message);
    }
});

//update Document
router.post("/document/updateDocument", function (req, res) {
    Logger.logMsg("Document to be updated ");
    Logger.logObj(req.body);
    Logger.logObj(req.params);
    let newDocument = new Document(req.body);
    Logger.logMsg("About to update Document:");
    Logger.logObj(newDocument);
    Document.update({ _id: req.body._id }, req.body)
        .then(
            doc => {
                Logger.logMsgObj("doc::: ", doc);
                if (doc) Misc.respondCommonSuccess(res, "Success in updating Document", doc);
                else Misc.respondCommonError(res, "Error in updating Document");
            }
        )
        .catch(
            err => {
                console.log(err);
                Misc.respondCommonError(res, "Error in updating Document");
            }
        )
});

//get single document 
router.get("/document/getSingleDocument/:documentId", async function (req, res) {
    Logger.logMsg("Enter:getSingleDocument()");
    Logger.logObj(req.body);
    Logger.logObj(req.params);
    let documentId = req.params.documentId
    try {
        let document = await Document.findById(documentId);
        Logger.logMsgObj("got document: ", document);
        Misc.respondCommonSuccess(res, "success in getting document", document);
    } catch (err) {
        Misc.respondCommonError(res, "Error in getting document", err.message);
    }
});


//delete Document
router.post("/document/deleteDocument/:documentId", function (req, res) {
    Logger.logMsg("Document to be deleted ");
    Logger.logObj(req.params);
    Document.deleteOne({ _id: req.params.documentId})
        .then(
            doc => {
                Logger.logMsgObj("doc::: ", doc);
                if (doc) Misc.respondCommonSuccess(res, "Success in deleting Document", doc);
                else Misc.respondCommonError(res, "Error in deleting Document");
            }
        )
        .catch(
            err => {
                console.log(err);
                Misc.respondCommonError(res, "Error in deleting Document");
            }
        )
});
///////////////////////////// document - end








///////////////////////////// class - end
module.exports = router; 