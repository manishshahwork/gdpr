const mongoose = require("mongoose");

const OrganizationSchema = mongoose.Schema({
    name: { type: String, default:"" },
    street: { type: String, default:"" },
    zip: { type: String, default:"" },
    city: { type: String, default:"" },
    phone: { type: String, default:"" },
    country: { type: String, default:"" },
    email: { type: String, default:"" },
    icon: { type: String, default:"" },
    website: { type: String, default:"" },
    is_template: {type:Boolean, default:false}
});

const Organization = module.exports = mongoose.model("Organization", OrganizationSchema, "organizations");
