const mongoose = require("mongoose");

const ActivitySchema = mongoose.Schema({
    status: { type: String },
    label: { type: String, default: "" },
    purpose: { type: String, default: "" },
    role: { type: String },
    department: { type: String, default: "" },
    process_owner: { type: String, default: "" },
    storage_period: { type: String, default: "" },
    last_impact_assessment_date: { type: Number },
    data_deletion_process: { type: String, default: "" },
    data_portability_process: { type: String, default: "" },
    data_minimization_process: { type: String, default: "" },
    created_date: { type: String, default: "" },
    is_template: { type: Boolean },

    is_legal_public_interest: { type: Boolean, default: false },
    legal_text_public_interest: { type: String, default: "" },
    is_legal_consent: { type: Boolean, default: false },
    legal_text_consent: { type: String, default: "" },
    is_legal_contract: { type: Boolean, default: false },
    legal_text_contract: { type: String, default: "" },

    is_access_ceo: { type: Boolean, default: false },
    is_access_ciso: { type: Boolean, default: false },
    is_access_counsel: { type: Boolean, default: false },

    is_toms_ability_it: { type: Boolean, default: false },
    is_toms_alarm: { type: Boolean, default: false },
    is_toms_automatic_logging: { type: Boolean, default: false }

});

const Activity = module.exports = mongoose.model("Activity", ActivitySchema, "activities");
