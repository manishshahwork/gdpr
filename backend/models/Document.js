const mongoose = require('mongoose');

const DocumentSchema = mongoose.Schema({
    created_date: { type: String, default: "" },
    label: { type: String, default: "" },
    file_name: { type: String, default: "" },
    file_url: { type: String, default: "" },
    details: { type: String, default: "" },
    status: { type: String, default: "" },
    document_date: { type: Number, default: -1 },
    resubmission: { type: Number, default: -1 },
    type: { type: String, default: "" },
    confidentiality: { type: String, default: "" },
    iso_reference: { type: String, default: "" },
    permissions: { type: String, default: "" }
});

const Document = module.exports = mongoose.model("Document", DocumentSchema, "documents");