const mongoose = require('mongoose');

const ContactSchema = mongoose.Schema({
    salutation: { type: String, default: '' },
    first_name: { type: String, default: "" },
    last_name: { type: String, default: "" },
    email: { type: String, default: "" },
    role: { type: String, default: "" },
    phone: { type: String, default: "" },
    organization_id: { type: String, default: "" }
});

const Contact = module.exports = mongoose.model("Contact", ContactSchema, "contacts");