const mongoose = require('mongoose');

const UserSchema = mongoose.Schema({
    login_email: { type: String, default: "" },
    permissions: { type: String, default: "" },
    ui_language: { type: String, default: "" },
    contact_id: { type: String, default: "" },
    status: { type: String, default: "Active" }
});

const User = module.exports = mongoose.model("User", UserSchema, "users");