const mongoose = require("mongoose");
const Contact = require('./models/Contact');
const Organization = require('./models/Organization');
const Logger = require('./config/Logger');
const Activity = require ('./models/Activity');
class Operations {


    ///////////////////////////// organization - start
    static addOrganization(newOrganization, callback) {
        return newOrganization.save(callback);
    }

    static getAllOrganizations(isTemplate) {
        Logger.logMsg("inside getAllOrganizationTemplates");
        let query = Organization.find();
        query.where('is_template').equals(isTemplate);
        return query.exec();
    }

    ///////////////////////////// organization - end


    ///////////////////////////// contact - start
    static addContact(newContact, callback) {
        let c = new Contact(newContact);
        return c.save(callback);
    }

    //get matching contacts
    static getMatchingContacts(organizationId) {
        Logger.logMsg("inside getMatching... id:" + organizationId);
        let query = Contact.find();
        query.where('organization_id').equals(organizationId);
        // query.where('first_name').equals("Rick");
        //execute the query
        return query.exec();
    }
    ///////////////////////////// contact - end

    ///////////////////////////// user - start
    static addUser(newUser, callback) {
        return newUser.save(callback);
    }

    ///////////////////////////// user - end

    ///////////////////////////// activity - start
    static addActivity(newActivity, callback) {
        return newActivity.save(callback);
    }

    static getAllActivities(isTemplate) {
        Logger.logMsg("inside getAllActivities");
        let query = Activity.find();
        query.where('is_template').equals(isTemplate);
        return query.exec();
    }

    ///////////////////////////// activity - end
    

     ///////////////////////////// document - start
     static addDocument(newDocument, callback) {
        return newDocument.save(callback);
    }

    ///////////////////////////// document - end

    // static getItemLogByLogDate(logDate, callback) {
    //     let query = ItemLog.find();
    //     query.where('log_date').equals(logDate);
    //     // query.where('log_date').gte(logDate);
    //     // query.where('log_date').lte(logDate);
    //     ItemLog.findOne(query, callback);
    // }

    // static addItemLog(newItemLog, callback) {
    //     newItemLog.save(callback);
    // }


    // //search
    // static getSpecificItemLogs(searchStartTime, searchEndTime, searchFeedbackType, searchAnswerType) {
    //     let query = ItemLog.find();
    //     if (searchStartTime != -1) query.where('log_date').gte(searchStartTime);
    //     if (searchEndTime != -1) query.where('log_date').lte(searchEndTime);
    //     if (searchFeedbackType != -1) query.where('is_liked').equals(this.getBooleanValueFromNumber(searchFeedbackType));
    //     if (searchAnswerType != -1) query.where('is_answered').equals(this.getBooleanValueFromNumber(searchAnswerType));

    //     //execute the query
    //     return query.exec();

    // }

    // static addManyItemLogs(itemLogsArr) {
    //     return ItemLog.insertMany(itemLogsArr);
    // }

    // static updateManyItemLogs(itemLogsArr) {
    //     return ItemLog.updateMany(itemLogsArr);
    // }
    // static deleteItemLog(id) {
    //     console.log("id::::" + id);
    //     return ItemLog.findOneAndRemove(id);
    // }

}
module.exports = Operations;