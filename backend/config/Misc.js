const Logger = require("../config/Logger");

class Misc {
    static respondCommonSuccess(res, msg = "", retData = null) {
        Logger.logMsg(msg);
        if(msg == null || msg == "") msg = "Success";
        res.status(200).json({
            status: 200, message: msg , data: retData
        });
    }

    static respondCommonError(res, msg = "", retData = null) {
        Logger.logMsg(msg);
        if(msg == null || msg == "") msg = "Error";
        res.status(500).json({
            status: 500, message: msg , data: retData
        });
    }
}

module.exports = Misc;